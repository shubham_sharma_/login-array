var joi=require('joi');
var nodemailer=require('nodemailer');
var config=require('../Config/Config');

var crypto=require('crypto');
var privateKey=config.key.privateKey;


var smtpTransport = nodemailer.createTransport( {
    service: "gmail",
    auth: {
        user:config.email.username ,
        pass:config.email.password
    }
});
module.exports.sentMailVerificationLink = function(user,token) {
       var from = config.email.accountName+" Team<" + config.email.username + ">";
    var mailbody = "<p>Thanks for Registering on "+config.email.accountName+" </p><p>Please verify your email by clicking on the verification link below.<br/><a href='http://"+config.server.host+":"+ config.server.port+"/"+config.email.verifyEmailUrl+"/"+token+"'>Verification Link</a></p>";
           mail(from, user , "Account Verification", mailbody);
    };


function mail(from,email,subject,mailbody)
{
var mailOptions={
    from:from,
    to:email,
    subject:subject,
    html:mailbody

}
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log(error);
        }else{
            console.log("Message sent: " + response.message);
        }

        smtpTransport.close();
    });

}

module.exports.decrypt = function(password) {
    return decrypt(password);
}
module.exports.encrypt = function(password) {
    return encrypt(password);
}

function decrypt(password) {
       var decipher = crypto.createDecipher('aes-256-ctr', privateKey);
       var dec = decipher.update(password, 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    }
function encrypt(password) {
        var cipher = crypto.createCipher('aes-256-ctr', privateKey);
        var crypted = cipher.update(password, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return crypted;
    }



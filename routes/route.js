var joi=require('joi');
var common=require('../controller/common');
var hapiSwagger=require('hapi-swagger');
var config=require('../Config/Config');
var privateKey=config.key.privateKey;
var jwt=require('jsonwebtoken');
var User=require('../Service/datao');
var  ArrayUser=require('../Service/arrayo');
var qs=require('../controller/quickSort');






var geek1={
    method:'POST',
    path:'/register',
    handler:function(request,reply)
    {

request.payload.password=common.encrypt(request.payload.password);
        var tokenData={
            userName:request.payload.name,

        };
        var token=jwt.sign(tokenData,privateKey,{algorithm:'HS256'});
       User.storeUser(request.payload,token);





common.sentMailVerificationLink(request.payload.email,token);

        reply('You have been Successfully registered! Congratulations...Please Confirm your email by clicking in your email');
        console.log(request.payload.name);
        console.log(request.payload.email);
        console.log(request.payload.password);
        console.log(token);
    },
    config:
    {
        tags:['api'],
        validate:{
            payload:{
                name:joi.string().required(),
                email:joi.string().required().email(),
                password:joi.string().required(),
                age:joi.number().required().min(18),
                phoneNumber:joi.number().required()
            }

        }
    }

};





var geek2={
method:'GET',
    path:'/verifyEmail/{token}',
    handler:function(request,reply) {
        var accessToken = request.params.token;
     User.findByToken(accessToken,function(ob){
         if(ob[0].isVerified===true){reply('You are Already Registered as an Authentic User');}
         User.updateVerification(ob[0].email,function(obj){
             reply("Congratulations You are Now An Authentic User");
         });


     });


    },
    config:{
        validate:{
            params:{
                token:joi.required()
            }
        }
    }




};

var geek3={
  method:'POST',
    path:'/store',
    config:{
        tags:['api'],
        validate:{
            payload:{
                userName:joi.string().required(),
                Target:joi.array().required(),
            }
        }

    },
handler:function(request,reply)
{
    ArrayUser.storeArrayUser(request.payload);
    reply("Your Array has been to the database");
}



};

var geek4={
  method:'POST',
  path:'/findIndex/{username}/{value}',
  handler:function(request,reply){
      var namee=request.params.username;
      ArrayUser.findArray(namee,function (obj){
var index=obj[0].Target.indexOf(request.params.value);

if(index==-1){
    reply("Your value Does not exist")
}
          else{
    reply("Your value is at index "+index);
}

      });
  } ,
    config:{
        tags:['api'],
        validate:{
            params:{
                username:joi.string().required(),
                value:joi.number().required()
            }
        }
    }




};

var geek5={
  method:'POST',
path:'/push/{username}/{value}',
    handler:function(request,reply)
    {
       ArrayUser.Concatenate(request.params.username,request.params.value,function(obj){
           ArrayUser.findArray(request.params.username,function(ob1){
               reply("Your updated array in databas eafter the push operation is "+ob1[0].Target);
           });

       });
    },
    config:{
        tags:['api'],
        validate:{
            params:{
                username:joi.string().required(),
                value:joi.number().required()
            }
        }
    }



};

var geek6={
    method:'POST',
    path:'/delete/{username}/{value}',
    handler:function(request,reply)
    {

ArrayUser.delete(request.params.username,request.params.value,function(obj){
   ArrayUser.findArray(request.params.username,function(ob1){
      reply("Your Array After the deletion is "+ob1[0].Target);
   });
});
    },
    config:{
        tags:['api'],
        validate:{
            params:{
                username:joi.string().required(),
                value:joi.number().required()
            }
        }
    }



};
var geek7={
  method:'POST',
    path:'/sort/{username}',
    handler:function(request,reply){
        ArrayUser.findArray(request.params.username,function(obj){
           var arr=obj[0].Target;
            qs.quickSort(arr,0,arr.length-1);
  ArrayUser.updateById(request.params.username,arr,function(objsd){
reply("Your Updated Sorted Array is "+arr);
  });
        });
    },
    config:{
        tags:['api'],
        validate:{
            params:{
                username:joi.string().required()
            }
        }
    }

};

var geek8={
    method:'GET',
    path:'/login/{username}/{password}',
    handler:function(request,reply)
    {

 User.findByEmail(request.params.username,function(obj){
    var password=obj[0].password;
    if(obj[0].isVerified===false){
        reply("Sir You are not an authentic User. Please Click the link in your email");
    }
     else{
        if(obj[0].password===common.encrypt(request.params.password)){
            reply("Ok Now You Are Logged In!! Boom I will send you a token to communicate");
        }
        else{
            reply("Sir Your Password is wrong ....Please Reneter your Password");
        }
    }
 });









       // reply('You have been Successfully logged in! Congratulations...Please Confirm your email by clicking in your email');

    },
    config:
    {
        tags:['api'],
        validate:{
            params:{

                username:joi.string().required().email(),
                password:joi.string().required(),

            }

        }
    }

};








var jeal=[geek1,geek2,geek3,geek4,geek5,geek6,geek7,geek8];
module.exports.deal=jeal;
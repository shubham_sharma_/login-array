var hapi=require('hapi');
var server=new hapi.Server();
var hapiSwagger=require('hapi-swagger');
var inert=require('inert');
var vision=require('vision');
var Routes=require('./routes/route');
var config=require('./Config/Config');
var privateKey=config.key.privateKey;
var ttl=config.key.tokenExpiry;
var jwt=require('jsonwebtoken');
var Moment=require('moment');






server.connection({
    host:'localhost',
    port:8000
});

var geek={
    register:hapiSwagger,
    //options:SWAGGER_OPTIONS
};
var arr=[inert,vision,geek];

server.register(arr,function(error)
{
    if (error)
        throw error;
});



var validate = function (request, decodedToken, callback) {


    var diff = Moment().diff(Moment(token.iat * 1000));
    if(diff>ttl)
    {return callback(error,false);}
    return callback(error, true);
};



server.register(require('hapi-auth-jwt'), function (error) {

    server.auth.strategy('token', 'jwt', {
        key: privateKey,
        validateFunc: validate,
        verifyOptions: {algorithms: ['HS256']}  // only allow HS256 algorithm
    });
});


server.route(Routes.deal);

server.start(function()
{
    console.log('Your server is running at: '+server.info.uri);
});

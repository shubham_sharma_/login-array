var mongoose=require('mongoose');
var db=require('./config').db;


var Schema=mongoose.Schema;
var sch=new Schema({

  index:{
      type:Number,
      required:true,
      unique:true
  },
    value:{
        type:Number,
        required:true
    }

});


var ArraSchema=new Schema({

   userName:{
       type:String,
       required:true,
       unique:true
   },
    Target:{
       type:Array,
       default:[]
   }


});

var ArrayUser=mongoose.model('ArrayUser',ArraSchema);
module.exports=ArrayUser;

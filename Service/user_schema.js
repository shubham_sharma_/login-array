var mongoose=require('mongoose');

var db=require('./config').db;

var Schema=mongoose.Schema;
var objectId=Schema.objectId;


var UserSchema=new Schema({

    name:{
        type:String,
        required:true
    },

    email:{
        type:String,
        required:true,
        unique:true

    },
    password:{
      type:String,
        required:true
    },
age:{
    type:Number,
    min:18,
    index:true
},
 phoneNumber:{
     type:Number,
     required:true
 },
  createdAt:Date,

  upDatedAt:Date,
    isVerified:{
        type:Boolean,
        default:false
    },
    token:{
        type:String,
        required:true
    }
});


///Adding Custom Methods to our USerSchema Model


var User=mongoose.model('User',UserSchema);

module.exports=User;

var mongoose=require('mongoose');

var db=mongoose.connection;

mongoose.connect('mongodb://localhost/freshdb');
db.on('error',console.error.bind(console,'connection error'));
db.once('open',function callback()
{
   console.log("Connection with database succeeded");
});

module.exports.mongoose=mongoose;
module.exports.db=db;
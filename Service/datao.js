var User=require('./user_schema');
var tok=require('./token_Schema');


module.exports.storeUser=function(Target,Token)
{

 var newUser=new User(Target);

    newUser.pre('save',function(next){

  var currentDate=new Date();
        this.upDatedAt=currentDate;
        if(!this.createdAt)
        {
            this.createdAt=currentDate;
        }
        this.token=Token;
        next();
    });

    newUser.save(function(err)
    {
       if(!err)
       {console.log("User Successfully Added to the database");}
    });
}

module.exports.findByToken=function(Token,callback)
{
User.find({"token":Token},function(err,user){
    if(err){throw err;}
    return callback(user);

});


};


module.exports.findByEmail=function(email,callback){
    User.find({"email":email},function(err,user){
       if(err){throw err;}
        return callback(user);
    });

};

module.exports.updateVerification=function(email,callback){
    User.update({"email":email},{$set:{"isVerified":true}},function(err,user){
       if(err){throw err;}
        callback(user);
    });
}






